# GPS set-up for Raspberry Pi
## 1. Install GPSD
`sudo apt-get install gpsd gpsd-clients`

## 2. Stop and disable gpsd.socket to configure it
`sudo systemctl stop gpsd.socket`

`sudo systemctl disable gpsd.socket`

## 3. Identify GPS device
`ls /dev/tty*` and then look for ttyUSB0 or ttyAMA0 or something similar. 

## 4. Configure GPSD with the correct device path
`sudo gpsd /dev/ttyUSB0 -F /var/run/gpsd.sock` replacing ttyUSB0 with the correct filepath.

## 5. Run GPSD
`cgps`