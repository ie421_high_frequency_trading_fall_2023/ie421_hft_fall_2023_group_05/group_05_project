# PiGPS System Components

## 1. GPS Data Collection Nodes (Raspberry Pi units with GPS receivers)

- **GPS Receiver (U-blox Module)**:
  - Responsible for capturing satellite signals and determining location/time.
  - Communicates with the Raspberry Pi over UART or USB.

- **GPS Data Interface (Python Script)**:
  - Reads data from the GPS receiver.
  - Optionally formats or preprocesses this data for transmission.
  - Sends configuration UBX commands to the GPS module upon receiving them from the master server.

- **TCP Client**:
  - Maintains a connection with the master server.
  - Transmits the GPS data to the master server.
  - Listens for configuration commands from the master server to be sent to the GPS module.

## 2. Master Server

- **TCP Server**:
  - Listens for connections from multiple Raspberry Pi units.
  - Receives GPS data and stores it in the database.
  - Sends configuration commands to Raspberry Pi units upon requests from the web dashboard.

- **SQL Database**:
  - Stores received GPS data.
  - Can also maintain logs of configuration changes and other metadata.

- **Web Server (e.g., Flask, Django)**:
  - Serves the web dashboard to users.
  - Provides an interface for viewing stored GPS data.
  - Offers controls for sending configuration commands to connected Raspberry Pi units.

- **Web Dashboard (Frontend)**:
  - Displays the GPS data in a 3D-format using [CesiumJS](https://cesium.com/platform/cesiumjs/).
  - Provides an interface for users to select and send UBX configuration commands to specific Raspberry Pi units.
  - Shows status or acknowledgment messages resulting from configuration commands.

## 3. User Interaction

- **View Mode**:
  - Users access the web dashboard to view current GPS & observable satellite locations using [CesiumJS](https://cesium.com/platform/cesiumjs/).
  - Show weather information in local areas (need to discuss this).

- **Configuration Mode**:
  - Users can select specific Raspberry Pi units from the dashboard.
  - They can then choose or input UBX commands to be sent to the GPS module of that unit.
  - Feedback from the command (e.g., success or error messages) is displayed.

## Optional Enhancements

- **Authentication & Authorization**:
  - User accounts and roles can be added to ensure only authorized individuals can access the dashboard and send configuration commands.

- **Data Filtering & Analysis**:
  - Tools or features to analyze the GPS data, filter it, or export it in different formats.

- **Alerts & Notifications**:
  - System can send alerts for specific events, like a Raspberry Pi unit going offline, a GPS module malfunction, or configuration errors.
