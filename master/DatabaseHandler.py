import psycopg2
from psycopg2 import sql
import os
from ast import literal_eval as make_tuple
from termcolor import colored

DEBUG_MODE = True

class DatabaseHandler:

    def __init__(self):
        self.connection = psycopg2.connect(
            dbname='postgres',
            user='postgres',
            password='password',
            host='pigps.co84fzgsmbvq.us-west-2.rds.amazonaws.com',
            port='5432'
        )
        self.cursor = self.connection.cursor()

    def create_tables(self):
        commands = (
            """
            CREATE TABLE nodes (
                node_id VARCHAR(255) PRIMARY KEY,
                ip_address VARCHAR(255) NOT NULL,
                last_seen TIMESTAMP NOT NULL,
                status VARCHAR(50) NOT NULL,
                location POINT,
                timestamp TIMESTAMP
            )
            """,
            """
            CREATE TABLE satellites (
                satellite_id INTEGER PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                latest_coordinates POINT
            )
            """
        )

        for command in commands:
            self.cursor.execute(command)
        
        self.connection.commit()

    def list_all_tables(self):
        self.cursor.execute("""
            SELECT table_name
            FROM information_schema.tables
            WHERE table_schema = 'public'
        """)
        tables = self.cursor.fetchall()
        print("Tables:")
        for table in tables:
            print(table[0])

    def delete_all_tables(self):
        self.cursor.execute("""
            SELECT table_name
            FROM information_schema.tables
            WHERE table_schema = 'public'
        """)
        tables = self.cursor.fetchall()
        
        for table in tables:
            self.cursor.execute(f"DROP TABLE IF EXISTS {table[0]} CASCADE;")
            
        self.connection.commit()
        print("All tables in the 'public' schema have been deleted.")

    def list_all_nodes(self):
        self.cursor.execute(
        """
            SELECT * FROM nodes 
            WHERE timestamp > NOW() - INTERVAL '5 minutes'
        """
        )
        nodes = self.cursor.fetchall()
        nodes_list = []
        print("got nodes: ", nodes_list)
        for node in nodes:
            print(node)

            location = make_tuple(node[4])

            node_dict = {
                "node_id": node[0],
                "ip_address": node[1],
                "last_seen": node[2],
                "status": node[3],
                "location": {"latitude": location[0], "longitude": location[1]} if node[4] else None,
                "timestamp": node[5]
            }
            nodes_list.append(node_dict)
        return nodes_list


    def list_all_satellites(self):
        self.cursor.execute("SELECT * FROM satellites")
        satellites = self.cursor.fetchall()
        satellites_list = []
        
        for satellite in satellites:
            location = make_tuple(satellite[2])
            satellite_dict = {
                "satellite_id": satellite[0],
                "name": satellite[1],
                "latest_coordinates": {"elevation": location[0], "azimuth": location[1]} if satellite[2] else None
            }
            if satellite_dict["latest_coordinates"]["elevation"] == -999 or satellite_dict["latest_coordinates"]["azimuth"] == 0:
                continue
            satellites_list.append(satellite_dict)
        return satellites_list


    def get_node_data(self, node_id):
        self.cursor.execute("SELECT * FROM nodes WHERE node_id = %s", (node_id,))
        node_data = self.cursor.fetchone()
        if node_data:
            data = {
                "node_id": node_data[0],
                "ip_address": node_data[1],
                "last_seen": node_data[2],
                "status": node_data[3],
                "location": {"latitude": node_data[4][0], "longitude": node_data[4][1]},
                "timestamp": node_data[5]
            }
            return data
        else:
            return None  

    def insert_node(self, node_id, ip_address, last_seen, status, location, timestamp):        
        # Handle 'Unknown' values
        if location[0] == "Unknown":
            location = (0.0, 0.0)  # set default coordinates
        if timestamp == "Unknown":
            timestamp = None

        command = """
        INSERT INTO nodes (node_id, ip_address, last_seen, status, location, timestamp) 
        VALUES (%s, %s, %s, %s, point(%s, %s), %s) 
        ON CONFLICT (node_id) DO UPDATE SET
            ip_address = EXCLUDED.ip_address,
            last_seen = EXCLUDED.last_seen,
            status = EXCLUDED.status,
            location = EXCLUDED.location,
            timestamp = EXCLUDED.timestamp
        RETURNING node_id
        """

        # print("got location:", location, "with type", type(location))
        # location = make_tuple(location)
        
        try:
            self.cursor.execute(command, (node_id, ip_address, last_seen, status, location[0], location[1], timestamp))
        except Exception as e:
            print("Error during insert: ", e)
            self.connection.rollback() 
            return None
        
        node_id = self.cursor.fetchone()[0]
        self.connection.commit()
        
        if DEBUG_MODE:
            self.log(f"Inserted node with ID {node_id} at location {location}")
        return node_id

    def insert_satellite(self, prn, name, latest_coordinates):
        # Handle 'Unknown' values
        if latest_coordinates[0] == "Unknown" or latest_coordinates[1] == "Unknown":
            latest_coordinates = (0.0, 0.0)  # set default coordinates

        command = """
        INSERT INTO satellites (satellite_id, name, latest_coordinates) 
        VALUES (%s, %s, point(%s, %s)) 
        ON CONFLICT (satellite_id) DO UPDATE SET
            name = EXCLUDED.name,
            latest_coordinates = EXCLUDED.latest_coordinates
        RETURNING satellite_id
        """


        try:
            self.cursor.execute(command, (prn, name, latest_coordinates[0], latest_coordinates[1]))
        except Exception as e:
            print("Error during insert: ", e)
            self.connection.rollback() 
            return None
        
        satellite_id = self.cursor.fetchone()[0]
        self.connection.commit()
        
        if DEBUG_MODE:
            self.log(f"Inserted satellite with ID {prn}")        

        return satellite_id

    def print_all_satellites(self):
        self.cursor.execute("SELECT * FROM satellites")
        satellites = self.cursor.fetchall()
        print("Satellites:")
        for satellite in satellites:
            print(satellite)

    def close(self):
        self.cursor.close()
        self.connection.close()

    def log(self, msg, prefix="[DatabaseHandler]"):
        print(colored(prefix, 'yellow', attrs=['bold']), msg)
