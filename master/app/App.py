from flask import Flask, jsonify, request
from flask_cors import CORS
import requests
import sys
import asyncio
sys.path.append("..")
from DatabaseHandler import DatabaseHandler  
import socket
import json

app = Flask(__name__)
CORS(app)
db = DatabaseHandler()

COMMAND_PORT = 8887


OPENWEATHERMAP_API_KEY = '89a3d39c6d2d65914a457b42689c5f13'
WEATHER_API_URL = 'http://api.openweathermap.org/data/2.5/weather'

@app.route('/send-command', methods=['POST'])
def send_command():
    command_info = request.get_json()
    # Assuming command_info contains 'node_id' and 'ubx_command'

    try:
        with socket.create_connection(("localhost", COMMAND_PORT)) as sock:  # Replace localhost with TCPCommServer's address
            sock.sendall(json.dumps(command_info).encode())
            response = sock.recv(10000)  # Adjust buffer size as needed
            # Decode the response from bytes to string
            response_str = response.decode()            

            # Parse the string into a JSON object
            print("GOT RESPONSE: ", response_str)
            response_json = json.loads(response_str)
            # html_formatted = response_json["response"].replace("\n", "<br>")
            # response_json["response"] = html_formatted

            return jsonify(response_json)          
    except ConnectionError:
        return jsonify({"status": "error", "message": "Could not connect to TCPCommServer"})


@app.route('/nodes', methods=['GET'])
def get_all_nodes():
    nodes = db.list_all_nodes()
    return jsonify(nodes)

@app.route('/satellites', methods=['GET'])
def get_all_satellites():
    satellites = db.list_all_satellites()
    return jsonify(satellites)

@app.route('/gps-data/<int:node_id>', methods=['GET'])
def get_gps_data_by_node_id(node_id):
    data = db.get_node_data(node_id) 
    return jsonify(data)

@app.route('/weather', methods=['GET'])
def get_weather():
    latitude = request.args.get('lat')
    longitude = request.args.get('lon')

    if not latitude or not longitude:
        return jsonify({"status": "error", "message": "Missing latitude or longitude"}), 400

    params = {
        'lat': latitude,
        'lon': longitude,
        'appid': OPENWEATHERMAP_API_KEY,
        'units': 'metric'  # or 'imperial' for Fahrenheit
    }

    try:
        response = requests.get(WEATHER_API_URL, params=params)
        response.raise_for_status()  # Raise an error for bad status codes
        weather_data = response.json()
        weather_description = weather_data['weather'][0]['description']
        temperature = weather_data['main']['temp']
        return jsonify({"status": "success", "weather": weather_description, "temperature": temperature})
    except requests.RequestException as e:
        return jsonify({"status": "error", "message": str(e)}), 500


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)    