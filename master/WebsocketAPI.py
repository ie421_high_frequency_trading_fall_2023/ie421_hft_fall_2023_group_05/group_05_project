import asyncio
import websockets
import json
from DatabaseHandler import DatabaseHandler

async def api(websocket, path):
    db_handler = DatabaseHandler()
    try:
        async for message in websocket:
            data = json.loads(message)
            command = data.get("command")

            response = {}
            if command == "get_all_nodes":
                response["nodes"] = db_handler.get_all_nodes()
            elif command == "get_all_satellites":
                response["satellites"] = db_handler.get_all_satellites()                     

            await websocket.send(json.dumps(response))
    finally:
        db_handler.close()

start_server = websockets.serve(api, "0.0.0.0", 6789)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()