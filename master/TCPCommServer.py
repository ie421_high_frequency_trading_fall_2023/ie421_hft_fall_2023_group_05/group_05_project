import asyncio
import json
from termcolor import colored
from DatabaseHandler import DatabaseHandler
from datetime import datetime

class TCPCommServer:

    def __init__(self):
        self.connections = {}
        self.db = DatabaseHandler()      
        self.read_locks = {}                               

    def log(self, prefix, msg):
        print(colored(prefix, 'red', attrs=['bold']), msg)

    async def handle_node(self, reader, writer):
        # Get node info
        addr = writer.get_extra_info('peername')
        ip = addr[0]
        port = addr[1]
        print(f"Connected to {addr}")

        # Initial handshake: RPI will send UUID
        data = await reader.read(100)
        data_decoded = data.decode()
        uuid = None
        try:
            # Parse json and extract uuid
            identifier = json.loads(data_decoded)
            uuid = identifier['uuid']
            self.log("[Master]", f"New node {identifier['uuid']} from {addr}")
            self.connections[identifier['uuid']] = (reader, writer, True)
            self.read_locks[identifier['uuid']] = asyncio.Lock()
        except json.JSONDecodeError:
            self.log("[Master]", f"Invalid JSON received from {addr}: {data_decoded}")

        # Repeatedly read data from RPI
        while True:    
            async with self.read_locks[identifier['uuid']]:        
                # Wait until there's data to read            
                payload = ""

                if self.connections[identifier['uuid']][2] == False:
                    continue

                # Read data until newline
                while not payload.endswith("\n"):
                    data = await reader.read(1024)
                    if not data:
                        break
                    payload += data.decode()

                # Remove the newline character at the end
                payload = payload.strip()

                if not payload:
                    break                                                

                # Parse json payload
                try:
                    payload = json.loads(payload)                                         
                    if payload["payload_type"] == 'TPV':                    
                        # Record node info in db                    
                        self.db.insert_node(uuid, ip, datetime.now(), "connected", (payload["latitude"], payload["longitude"]), payload["time"])                                                        
                    elif payload["payload_type"] == 'SKY':                                     
                        # Record each observed satellite info in db
                        for satellite in payload["satellites"]:                        
                            self.db.insert_satellite(satellite["PRN"], satellite["PRN"], (satellite["Elevation"], satellite["Azimuth"]))                                        
                except json.JSONDecodeError:
                    self.log("[Master]", f"Invalid JSON received from {addr}: {payload}")
        
        print(f"Connection closed from {addr}")
        del self.connections[identifier]
        writer.close()
        await writer.wait_closed()

    async def handle_command(self, reader, writer):
        data = await reader.read(1024)
        command = data.decode()

        try:
            self.log("[Master]", f"Received UBX command: {command}")
            command_data = json.loads(command)            
            node_id = command_data['node_id']

            if node_id in self.connections:
                async with self.read_locks[node_id]:
                    # self.connections[node_id][2] = False
                    command_data["payload_type"] = "UBX"
                    node_writer = self.connections[node_id][1]
                    node_reader = self.connections[node_id][0]

                    # Write command to node
                    json_command = json.dumps(command_data)                    
                    node_writer.write(json_command.encode())

                    # Wait for response from node                    
                    try:    
                        payload = ""
                        print("node reader: ", node_reader)

                        # Read data until newline
                        while not payload.endswith('\n'):
                            data = await node_reader.read(1024)
                            print("got data: ", data)
                            if not data:
                                break
                            payload += data.decode()

                        payload = payload.strip()

                        self.log("[Master]", f"Received UBX response from client: {payload}")                    
                        writer.write(payload.encode())  # Send the response back to Flask server                        
                    except Exception as e:
                        print(f"Got error: {e}")
            else:
                writer.write(f"No node with id {node_id}".encode())

        except json.JSONDecodeError:
            writer.write(b"Invalid command format")

        await writer.drain()
        writer.close()

    async def run(self, host, port, command_port):
        server = await asyncio.start_server(self.handle_node, host, port)
        command_server = await asyncio.start_server(self.handle_command, host, command_port)

        async with server, command_server:
            await asyncio.gather(server.serve_forever(), command_server.serve_forever())
    
if __name__ == '__main__':    
    host = '0.0.0.0'
    port = 8888
    command_port = 8887

    server = TCPCommServer()
    asyncio.run(server.run(host, port, command_port))    