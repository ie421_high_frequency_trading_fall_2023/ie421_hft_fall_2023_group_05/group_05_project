import socket
import time
import json
import gps
import gpsd
from termcolor import colored
import select
import subprocess
import argparse

class Node:
    def __init__(self, uuid, master_server, master_port, gpsd_host, gpsd_port):
        self.UUID = uuid
        self.log("[Node]", f"My UUID is {self.UUID}")           

        # Connect to master
        self.TCP_IP = master_server
        self.TCP_PORT = master_port
        self.BUFFER_SIZE = 1024
        self.FREQUENCY = 10
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.TCP_IP, self.TCP_PORT))

        # Connect to gpsd daemon        
        self.session = gps.gps(gpsd_host, gpsd_port)
        self.session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
        gpsd.connect()

    def log(self, prefix, msg):
        print(colored(prefix, 'red', attrs=['bold']), msg)

    def stop_gpsd(self):
        subprocess.run(['sudo', 'systemctl', 'stop', 'gpsd.socket'])
        subprocess.run(['sudo', 'systemctl', 'stop', 'gpsd.service'])

    def start_gpsd(self):
        subprocess.run(['sudo', 'systemctl', 'start', 'gpsd.socket'])
        subprocess.run(['sudo', 'systemctl', 'start', 'gpsd.service'])

    def format_ubx_command(self, hex_string):
        """
        Convert a hex string into a comma-separated list of hex values.
        """
        # Split the hex string into chunks of two characters
        hex_pairs = [hex_string[i:i+2] for i in range(0, len(hex_string), 2)]    
        return ','.join(hex_pairs)

    def send_ubx_command(self, command):
        """
        Send a UBX command to the GPS device via gpsd using ubxtool and return the response.
        """
        try:
            # Create the ubxtool command string
            ubxtool_command = f"ubxtool -c {command}"
            self.log("[Node]", f"Running command {ubxtool_command}")

            # Execute command and capture the output
            result = subprocess.run(ubxtool_command, shell=True, check=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            # Return stdout / stderr
            return result.stdout

        except subprocess.CalledProcessError as e:
            # Handle errors in the subprocess
            print(f"Error sending UBX command: {e.stderr}")
            return e.stderr
        except Exception as e:
            print(f"General error: {e}")
            return str(e)

    def run(self):
        try:    
            handshake = {"payload_type": "handshake", "uuid": self.UUID}
            handshake = json.dumps(handshake)
            self.s.sendall(handshake.encode('utf-8'))    
            
            while True:                
                # Check for incoming data from master
                ready = select.select([self.s], [], [], 0.5)
                if ready[0]:
                    data = self.s.recv(self.BUFFER_SIZE).decode('utf-8')
                    command = json.loads(data)

                    if command['payload_type'] == 'UBX':
                        # Convert command to comma separated hex list
                        formatted_command = self.format_ubx_command(command['command'])
                        response = self.send_ubx_command(formatted_command)

                        response_data = {
                            "payload_type": 'UBX_RESPONSE',
                            "response": response
                        }
                        json_data = json.dumps(response_data, ensure_ascii=False) + '\n'
                        self.s.sendall(json_data.encode('utf-8'))
                        self.log("[Node]", f"Sending response: {json_data.encode('utf-8')}")                        

                # Repeatedly read data from GPS
                report = self.session.next()

                # check for time position velocity payload
                if report['class'] == 'TPV':
                    data_to_send = {
                        "payload_type": 'TPV',
                        "latitude": getattr(report, 'lat', 'Unknown'),
                        "longitude": getattr(report, 'lon', 'Unknown'),
                        "time": getattr(report, 'time', 'Unknown')
                    }                      
                    
                    json_data = json.dumps(data_to_send, ensure_ascii=False) + '\n'      
                    self.s.sendall(json_data.encode('utf-8'))    

                    self.log("[Node]", f"Sending TPV payload:{data_to_send}")              
                # check for satellite payload
                elif report['class'] == 'SKY':
                    satellites_list = []
                    
                    for sat in report['satellites']:                        
                        satellite_data = {
                            "payload_type": 'SKY',
                            'PRN': sat['PRN'],
                            'Elevation': sat['el'],
                            'Azimuth': sat['az'],
                            'Signal Strength': sat['ss']
                        }
                        satellites_list.append(satellite_data)
                    
                    # Convert the list of satellite data to JSON format
                    data_to_send = json.dumps({'payload_type': 'SKY', 'satellites': satellites_list}, ensure_ascii=False) + '\n'
                                
                    # Send the JSON data to master
                    self.s.sendall(data_to_send.encode('utf-8'))            

                    self.log("[Node]", f"Sending SKY payload:{data_to_send}")              

                # sleep  a bit before the next reading
                time.sleep(1 / self.FREQUENCY)

        except KeyboardInterrupt:    
            self.s.close()

def parse_arguments():
    parser = argparse.ArgumentParser(description='Node Configuration')
    parser.add_argument('--uuid', type=str, default=socket.gethostname(), help='Master server IP address')
    parser.add_argument('--master_server', type=str, default='35.87.28.243', help='Master server IP address')
    parser.add_argument('--master_port', type=int, default=8888, help='Master server port')        
    parser.add_argument('--gpsd_host', type=str, default="localhost", help='GPSD host')
    parser.add_argument('--gpsd_port', type=str, default="2947", help='GPSD port')
    parser.add_argument('--frequency', type=int, default=10, help='GPS data collection frequency in Hz.')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_arguments()
    node = Node(args.uuid, args.master_server, args.master_port, args.gpsd_host, args.gpsd_port)
    node.run()