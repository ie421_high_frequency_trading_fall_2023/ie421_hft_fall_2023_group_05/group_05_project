## Overview & Motivation
Electronic trading firms now operate data centers across the globe, each with unique GNSS satellite views. Our project aims to provide firms with a platform for comprehensive 3D visualization & management of geolocation data from multiple sources. We’ve devised a system consisting of a single master server that communicates with any # of Raspberry Pi nodes, each equipped with a GPS receiver, to aggregate location, satellite, and local weather information. Drawing inspiration from PyGPSClient, the goal is to offer a unified web dashboard that not only displays information from multiple receivers but also allows for remote configuration using u-blox commands. This centralized approach ensures firms can monitor and manage their global GNSS setups, addressing the need for visualization and configuration from a single interface. 

## [User Interface Mockup](https://www.figma.com/file/a65elyo8DCA2KBW89jyJcQ/Untitled?type=design&node-id=0%3A1&mode=design&t=0Cj9BilPG8ffLEZX-1)

## Targets
### Level 1:
- Web GUI can show one Raspberry Pi on a CesiumJS map
- Raspberry Pi can connect to an EC2 server and send pings to update the database
- Website is runnable locally and can connect to a MySQL database (AWS RDS)
### Level 2:
- Multiple Raspberry Pis can connect to the servers and display on the web GUI
- Web GUI includes locations of all observed satellites above the earth
- Web GUI includes local weather of all Raspberry Pis
### Level 3:
- Raspberry Pis can be configured through the web GUI through UBX configuration commands

## Work Distribution
Areas of focus:

 - **Andrew**
    - TCP connection on the Rpis with the Master server
    - Flask master server
    - Hardware integration
    - AWS EC2 and RDS
 - **Apoorva**
    - CI/CD pipeline (for automatic deployment)
    - Flask master server
    - Front end development
 - **Atharva**
    - Web Dashboard design 
    - Front end development
     - CesiumJS integration
 - **Parth**
     - Hardware interaction
     - Backend
     - TCP client / master
     - AWS stuff

We will be creating Gitlab milestones to keep track of our overall goals/areas of focus which will have the more specific tasks as issues that will be assigned to different team members depending on what needs to completed and what the teammate prefers to work on. The issues will be developed further depending on the milestone and as we get to learn more about the tasks that we need to complete and understand the GPS chips, U-blox commands further, and see the whole project coming together.

# Timeline

The following is a rough timeline of what needs to be done each week.

### Week 1: Initial Setup and Planning
- Preliminary Research and setup.
- Understand U-blox commands.
- Setting up a TCP client-server model.
- Setup Raspberry Pi units with basic GPS receivers. 
- Set up development environments.
- Break down tasks and create a task tracking board (milestones in gitlab).
- Study CesiumJS and its integration.

### Week 2-3: Core Development
#### GPS Data Interface
- Create Python scripts to read data from the GPS receiver.
- Implement UBX command sending features.
- Develop the TCP client to maintain a connection with the master server.
- Implement data transmission and configuration command receipt.

#### Master Server
- Develop the TCP server to listen and handle connections.
- Setup SQL database for data storage.

#### Web Server & Dashboard
- Setup the Flask web server.
- Start developing the basic layout of the web dashboard.

### Week 4: Visualization and Enhancement
#### Web Dashboard - CesiumJS
- Integrate CesiumJS for 3D visualization of GPS data.
- Implement observable satellite locations feature.

#### Local Weather Information
- Integrate a weather API to display local weather details.

#### Configuration Mode
- Develop a user interface for sending UBX commands.
- Display feedback from commands.

### Week 5: Testing and Refinement
#### Testing
- Perform unit tests on individual components.
- Start integration testing.

#### Refinement
- Address issues and bugs found during testing.
- Improve user interface and user experience based on feedback.

#### Optional Enhancements
- Begin implementing Authentication & Authorization.
- Start on Data Filtering & Analysis features.

### Week 6: Finalization and Deployment
#### Optional Enhancements 
- Finish Authentication & Authorization.
- Implement Alerts & Notifications.

#### Final Testing
- Complete any remaining integration testing.
- Perform user acceptance testing.

#### Deployment
- Deploy the solution to a production environment.

#### Final Presentation
- Compile documentation and user guides.
- Prepare a presentation to showcase the project.

## External Resources/Requirements
 - **U-blox GPS chips** 
 - **AWS** 

## Final Deliverable(s)

#### High-level Diagram
![High-level Diagram Overview](https://i.ibb.co/k4GSB9D/Blank-diagram-2.png)

#### 1. GPS Data Collection Nodes (Raspberry Pi units with GPS receivers)

- **GPS Receiver (U-blox Module)**:
  - Responsible for capturing satellite signals and determining location/time.
  - Communicates with the Raspberry Pi over UART or USB.

- **GPS Data Interface (Python Script or Equivalent)**:
  - Reads data from the GPS receiver.
  - Optionally formats or preprocesses this data for transmission.
  - Sends configuration UBX commands to the GPS module upon receiving them from the master server.

- **TCP Client**:
  - Maintains a connection with the master server.
  - Transmits the GPS data to the master server.
  - Listens for configuration commands from the master server to be sent to the GPS module.

#### 2. Master Server (AWS EC2 Instance)

- **TCP Server**:
  - Listens for connections from multiple Raspberry Pi units.
  - Receives GPS data and stores it in the database.
  - Sends configuration commands to Raspberry Pi units upon requests from the web dashboard.

- **PostgreSQL Database (AWS RDS)**:
  - Stores received GPS data.
  - Can also maintain logs of configuration changes and other metadata.

- **Web Server (e.g., Flask, Django)**:
  - Serves the web dashboard to users.
  - Provides an interface for viewing stored GPS data.
  - Offers controls for sending configuration commands to connected Raspberry Pi units.

- **Web Dashboard (Frontend)**:
  - Displays the GPS data in a 3D-format using [CesiumJS](https://cesium.com/platform/cesiumjs/).
  - Provides an interface for users to select and send UBX configuration commands to specific Raspberry Pi units.
  - Shows status or acknowledgment messages resulting from configuration commands.

#### 3. User Interaction

- **View Mode**:
  - Users access the web dashboard to view current GPS & observable satellite locations using [CesiumJS](https://cesium.com/platform/cesiumjs/).
  - Show weather information in local areas (need to discuss this).

- **Configuration Mode**:
  - Users can select specific Raspberry Pi units from the dashboard.
  - They can then choose or input UBX commands to be sent to the GPS module of that unit.
  - Feedback from the command (e.g., success or error messages) is displayed.

  ## Team members
  1. Atharva Naik (May 2024), applying to grad school
  2. Parth Saxena (May 2024), graduate student
  3. Andrew Shusterman (BS/MCS Dec 2024)
  4. Apoorva Aditya (May 2024)
