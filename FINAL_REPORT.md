# GeoPi

Electronic trading firms now operate data centers across the globe, each with unique GNSS satellite views. GeoPi aims to provide firms with a web-based platform for 3D visualization & management of geolocation data from multiple sources. The goal is to offer a unified web dashboard that not only displays information from multiple receivers but also allows for remote configuration using u-blox commands.

## Installation Details

### Setting up Access Tokens

Create a `.env` file locally in the `website` directory to store the following:

```
CESIUM_ION_ACCESS_TOKEN=<your_token_here>
API_BASE_URL=<your_api_url_here>
```

### Running the website

1. `cd` into the website directory
2. `npm install`

For development:

3. `npm run dev`

For production:

3. `npm run build`
4. `npm start`

### Running the Master TCP server

1. In the base directory, `pip install requirements.txt`
2. Run `python3 master/TCPCommServer.py`

### Running the Node (on devices like Raspberry Pis)

1. In the base directory, `pip install requirements.txt`
2. Run `python3 node/Node.py`

### Running the Flask API on Master server

1. In the base directory, `pip install requirements.txt`
2. Run `python3 master/app/App.py`

## How does it work?

![High-level Diagram Overview](images/system_architecture.png)

We host the TCP communication server, the Flask API, and the website on an EC2 instance connected to a PostgreSQL RDS instance. This trio is the powerhouse of the project, handling all communications with devices, storing all data in the database, and serving the frontend.

Each Raspberry Pi device runs a script that allows it to send GPS data to the TCP server. The server then stores this data in an SQL database on RDS. The Flask API serves as the communication channel between the frontend and the SQL database/TCP server. The website recieves data from the API and can also send u-blox configuration commands back to the API which is then sent to the server to execute on the corresponding device.

The website uses CesiumJS with Cesium ION to display a 3D rendered globe with various entities like satellites and connected devices. The internal API uses the OpenWeatherMap API to fetch weather data for each device location.

![Website Overview](images/website_1.png)

Connected devices can be configured using u-blox commands sent via [`ubxtool`](https://manpages.ubuntu.com/manpages/focal/man1/ubxtool.1.html). The frontend will display outputs from `stdout` and `stderror` in the device cards.

![Configuration Overview](images/website_2.png)

## Base Directory File Descriptions

### `node/Node.py`

-   **Functionality:** Driver program on the Node device to connect to Master server and receive/send data.
-   **Key Features:** Reads & sends GPS data to Master server, listens & executes remote UBX commands and forwards output to Master server and frontend.

### `master/app/App.py`

-   **Functionality:** Main Flask application server handling API endpoints for GPS data, satellite information, weather data, and command sending.
-   **Key Features:** Command sending to Raspberry Pi nodes, node and satellite data retrieval, and integration with OpenWeatherMap API for weather information.

### `master/DatabaseHandler.py`

-   **Functionality:** Handles database operations using PostgreSQL.
-   **Key Features:** Creating and managing database tables, listing and deleting tables, and CRUD operations for nodes and satellites.

### `master/TCPCommServer.py`

-   **Functionality:** Manages TCP connections with Raspberry Pi nodes and handles node data and UBX command transmission.
-   **Key Features:** Handshaking with Raspberry Pi nodes, data parsing and storage, and execution of UBX commands.

## Website Directory File Descriptions

These Svelte components form the core of the web frontend for a geolocation data management and visualization application. They provide an interactive interface to visualize devices and satellites on a map and manage them.

### `components/CesiumViewer.svelte`

-   **Functionality:** Manages the CesiumJS viewer for displaying GPS device and satellite locations on a 3D globe.
-   **Key Features:** Integration with CesiumJS, updating device and satellite locations, and creating markers for devices and satellites on the map. Also handles fly-ins and fly-outs on interactions with device cards in the sidebar.

### `components/DeviceCard.svelte`

-   **Functionality:** Represents a card for each GPS device, displaying its information and allowing command interaction.
-   **Key Features:** Displaying device details, fetching weather data for device locations, sending commands to devices, and handling user interactions. Also shows acknowledgement and outputs from configuration commands.

### `components/SatelliteCard.svelte`

-   **Functionality:** Displays a connected satellite.
-   **Key Features:** Showing satellite PRNs along with relevant location information (elevation and azimuth).

### `App.svelte`

-   **Functionality:** Main application component that orchestrates the display and interaction with the CesiumViewer and DeviceCard components.
-   **Key Features:** Fetching device and satellite data, handling data refresh at regular intervals (every 10 seconds for devices and satellites, every 1 hour for weather), and managing user interactions with the device cards.

## Teammates

**Andrew Shusterman**

-   My name is Andrew Shusterman, and I'm a bachelor's student who will soon be pursuing an M.S. in Computer Science at the University of Illinois Urbana-Champaign. Throughout my academic and professional career, I have loved exploring all aspects of computer science, but have particularly enjoyed working with cloud infrastructure (AWS), machine learning, algorithms, and game design. I am most experienced with C++ and Python, but am also familiar with C, C#, SQL, JavaScript, HTML/CSS, and more. I have spent two summers interning at Intel, and am excited to learn more about the world of HFT.
-   [LinkedIn](https://www.linkedin.com/in/andrew-shusterman-a8b750201/), [Github](https://github.com/andrewsomeister)

**Apoorva Aditya**

-   My name is Apoorva Aditya, and I am a senior in Computer Science at the University of Illinois at Urbana-Champaign. I have experience working as a software developer intern at HERE Technologies and Fetch Rewards Inc. I have taken courses on Distributed Systems, Artificial Intellligence, Machine Learning, Interactive Computer Graphics, Database Systems, etc. I am most skilled in the Python, JavaScript, Dart, Kotlin, and C++ programming languages. I have experience working with Angular, React, Flutter, Android for frontend and Flask, Scala, Apache Flink, and Go for the backend. I am interested in learning about high frequency trading and other areas in finance too in general.

-   [LinkedIn](https://www.linkedin.com/in/apoorvaaditya/), [GitHub](https://github.com/ApoorvaAditya)

**Atharva Naik**

-   My name is Atharva Naik, and I'm a senior studying Mathematics & Computer Science at the University of Illinois Urbana-Champaign. I'm particularly interested in the mathematics of computer science (pure math, CS theory, and algorithms). I've taken courses on Artificial Intelligence, Machine Learning, Data Mining, Algorithms, Real Analysis, and Combinatorics. I have experience in Data Science and SWE working at Ameren and Panasonic respectively. I'm proficient in Python, Java, and C++, and I'm currently learning JavaScript.

-   [LinkedIn](https://www.linkedin.com/in/atharvanaik10), [GitHub](https://www.github.com/atharvanaik10)

**Parth Saxena**

-   My name is Parth Saxena, and I'm a graduate student pursuing an M.S. in Computer Science at the University of Illinois Urbana-Champaign. I've specialized in big data and artifical intelligence during my academic career, but I'm also greatly interested in the infrastructure behind high-frequency trading. I'm skilled in C/C++, Python, Java, and JavaScript. I have experience working with various database systems and cloud providers including AWS, GCP, and IBM Cloud. My work experiences have mostly been with startups including [Robin.io (Acquired by Rakuten)](https://www.robin.io/) and [Torch Sensors](https://www.torchsensors.com/).

-   [LinkedIn](https://www.linkedin.com/in/parthsaxena620/), [Github](https://github.com/parthsaxena)
